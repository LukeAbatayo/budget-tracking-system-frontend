import { useEffect, useState } from "react";
import Modal from 'react-modal';
import { Container, Row, Col, Button, Form } from 'react-bootstrap'
import ExpenseTable from './../components/ExpenseTable'

export default function Expense() {

    const [isAddModalOpen, setIsAddModalOpen] = useState(false)
    const [isEditModalOpen, setIsEditModalOpen] = useState(false)

    const [expenseCategories, setExpenseCategories] = useState([])

    const [newExpenseCategory, setNewExpenseCategory] = useState({
        category: ""
    })

    const [category, setCategory] = useState([])
    const [categoryId, setCategoryId] = useState({})

    const [expense, setExpense] = useState([])
    
    const [isAddExpenseModalOpen, setIsAddExpenseModalOpen] = useState(false);
    const [expenseId, setExpenseId] = useState();
    const [expenseAmount, setExpenseAmount] = useState("");
    const [isLoading, setIsLoading] = useState(false);
    
    const [isUpdateExpenseModalOpen, setIsUpdateExpenseModalOpen] = useState(false);
    const [expenseEntry, setExpenseEntry] = useState();
    const [expenseCategoryIdEntry, setExpenseCategoryIdEntry] = useState();
    const [selectedValue, setSelectedValue] = useState();
    const [newAmount, setNewAmount] = useState(0);
    const [entryId, setEntryId] = useState("");

    // Category

    // Retrieve Expense Category

    useEffect( () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {

            fetchCategories()

            return data
        })

    }, []);

    const fetchCategories = () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/expenseCategory', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            setExpenseCategories(data)
        })
    }

    // Create Expense Category

    const handleSubmit = e => {
        e.preventDefault()
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/expenseCategory', {
            method: "POST",
            body: JSON.stringify(newExpenseCategory),
            headers:{
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            isAddModalOpen(false)
            res.json()
        })
        .catch( err => console.log(err));
    }

    const handleChange = e => {
        setNewExpenseCategory({
            [e.target.id]: e.target.value
        })
    }

    // Update Expense Category

    const editExpenseCategory = (e, id, category) => {
        e.preventDefault()
        setIsEditModalOpen(true)
        setCategoryId(id)
        setCategory(category)
    }

    const handleExpenseCategoryChange = (e) => {
        setCategory(e.target.value)
    }

    const handleExpenseCategorySubmit = (e) => {
        e.preventDefault()

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/expenseCategory/${categoryId}`, {
            method : "PUT",
            body: JSON.stringify({"category": category}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    useEffect(() => {
        fetchCategories()
    }, [expenseCategories])

    // Delete Expense Category

    const deleteExpenseCategory = (e, id) => {
        e.preventDefault();
        
        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/expenseCategory/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    // Get Expense Entry

    useEffect( () => {
        
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {

            fetchExpense()

            return data
        })

    }, []);

    const fetchExpense = () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/expense', {
                headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
            setExpense(data)
        })
    }

    // Add Expense Entry

    const getExpenseCategory = (e) => {
        e.preventDefault()
        setExpenseId(e.target.value)
    }

    const handleExpenseEntrySubmit = (e) => {
        e.preventDefault()
        setIsLoading(false)

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${expenseId}/expense`, {
            method: "POST",
            body: JSON.stringify({"amount": expenseAmount}),
            headers:{
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            // setIsModalOpen(false)
            setIsLoading(true)
            res.json()
        })
        .catch( err => console.log(err));
    }

    useEffect(() => {
        if (isLoading) {
            fetchExpense()
        }
    }, [isLoading])

    // Edit Expense Entry

    const editExpenseEntry = (e, id, amount, category, categoryId) => {
        e.preventDefault()
        setIsUpdateExpenseModalOpen(true)
        setExpenseEntry(category)
        setExpenseCategoryIdEntry(categoryId)
        setEntryId(id)
    }

    const setNewCategory = (e) => {
        e.preventDefault()
        setSelectedValue(e.target.value)
    }

    const handleEditEntry = (e) => {
        e.preventDefault()
        setIsLoading(false)

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${expenseCategoryIdEntry}/expense/${entryId}`, {
            method : "PUT",
            body: JSON.stringify({"expenseCategoryId": {"_id": selectedValue}, "amount": newAmount}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    // delete entry

    const deleteEntry = (e, id, categoryId) => {
        e.preventDefault();
        setIsLoading(false)
        
        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${categoryId}/expense/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    return(
        <div>
            <Container>
                <div className="jumbotron">
                    <Row>
                        <Col lg={2} className="mb-5">

                            {/* Add Expense Categoty Button and Modal */}
                            <Button onClick={() => setIsAddModalOpen(true)}>Add Category</Button>
                            <Modal isOpen={isAddModalOpen} onRequestClose={() => setIsAddModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                                
                                <h2>Add Category</h2>
                                <Form onSubmit={handleSubmit}>

                                    <Form.Group controlId="category">

                                        <Form.Label>Category: </Form.Label>
                                        <Form.Control 
                                            type="text"
                                            onChange={handleChange}
                                            value={newExpenseCategory.category}
                                        />

                                    </Form.Group>
                                    
                                    <Button type="submit">Add</Button>
                                </Form>
                                
                                <Button onClick={() => setIsAddModalOpen(false)}>Cancel</Button>
                            </Modal>
                            
                        </Col>
                    </Row>
                    <Row>
                        <Col lg={12}>
                            <ExpenseTable expenseCategories={expenseCategories} editExpenseCategory={editExpenseCategory} deleteExpenseCategory={deleteExpenseCategory}/>

                            {/* Edit Expense Category Modal */}
                            <Modal isOpen={isEditModalOpen} onRequestClose={() => setIsEditModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                                <h2>Title</h2>
                                <Form onSubmit={handleExpenseCategorySubmit}>

                                    <Form.Group controlId="category">
                                        <Form.Label>Category: </Form.Label>
                                        <Form.Control 
                                            type="text"
                                            onChange={(event) => handleExpenseCategoryChange(event)}
                                            value={category}
                                        />
                                    </Form.Group>
                                    
                                    <Button type="submit">Update</Button>
                                </Form>
                                <Button onClick={() => setIsEditModalOpen(false)}>Cancel</Button>
                            </Modal>

                        </Col>
                    </Row>
                </div>
                <div className="jumbotron">
                    <Row>
                        <Col className="mb-5">

                            {/* Add Expense Entry Button and Modal*/}
                            <Button onClick={() => setIsAddExpenseModalOpen(true)}>Add Expense Entry</Button>
                            <Modal isOpen={isAddExpenseModalOpen} onRequestClose={() => setIsAddExpenseModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                                <h2>Title</h2>
                                <Form onSubmit={handleExpenseEntrySubmit}>

                                    <Form.Group controlId="category">
                                        <Form.Label>Category: </Form.Label>
                                        <Form.Control size="sm" as="select" onChange={getExpenseCategory}>
                                            <option></option>
                                            {   expenseCategories.map(expense => {
                                                    return (
                                                        <option value={expense._id}>{expense.category}</option>
                                                    )
                                                })
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    
                                    <Form.Group controlId="amount">
                                        <Form.Label>Amount: </Form.Label>
                                        <Form.Control 
                                            type="text"
                                            onChange={(event) => setExpenseAmount(event.target.value)}
                                            value={expenseAmount}
                                        />
                                    </Form.Group>
                                    
                                    <Button type="submit">Add</Button>
                                </Form>
                                <Button onClick={() => setIsEditModalOpen(false)}>Cancel</Button>
                            </Modal>
                            
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Category</th>
                                        <th>Price</th>
                                        <th colSpan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {expense.map(expense => {

                                        return (
                                            <tr key={expense._id}>
                                                <td>{expense.expenseCategoryId.category}</td>
                                                <td>{expense.amount}</td>
                                                <td>
                                                    <Button variant="warning" onClick={(event) => editExpenseEntry(event, expense._id, expense.amount, expense.expenseCategoryId.category, expense.expenseCategoryId._id)}>Edit</Button>
                                                </td>
                                                <td>
                                                    <Button variant="danger" onClick={(event) => deleteEntry(event, expense._id, expense.expenseCategoryId._id)}>Delete</Button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>

                            <Modal isOpen={isUpdateExpenseModalOpen} onRequestClose={() => setIsUpdateExpenseModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                                <h2>Title</h2>
                                <Form onSubmit={handleEditEntry}>

                                    <Form.Group controlId="category">
                                        <Form.Label>Category: </Form.Label>
                                        <Form.Control size="sm" as="select" onChange={setNewCategory}>
                                            <option></option>
                                            {   expenseCategories.map(expense => {
                                                    return (
                                                        <option value={expense._id}>{expense.category}</option>
                                                    )
                                                })
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    
                                    <Form.Group controlId="amount">
                                        <Form.Label>Amount: </Form.Label>
                                        <Form.Control 
                                            type="text"
                                            onChange={(event) => setNewAmount(event.target.value)}
                                            value={newAmount}
                                        />
                                    </Form.Group>
                                    
                                    <Button type="submit">Update</Button>
                                </Form>
                                <Button onClick={() => setIsUpdateExpenseModalOpen(false)}>Cancel</Button>
                            </Modal>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    )
}