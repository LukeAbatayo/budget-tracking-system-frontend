import RegisterForm from './../components/RegisterForm'
import { Container, Row, Col } from 'react-bootstrap'

export default function Register() {

    return (
        <Container>
            <Row>
                <Col className="mx-auto col-12 col-lg-6 mt-5">
                    <RegisterForm />
                </Col>
            </Row>
        </Container>
    )
}