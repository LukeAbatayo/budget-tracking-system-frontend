import { useEffect, useState } from "react";
import { Container, Row, Col, Button, Form } from 'react-bootstrap';
import Modal from 'react-modal';

Modal.setAppElement('#root');

export default function Income() {

    const [isModalOpen, setIsModalOpen] = useState(false)

    const [isEditModalOpen, setIsEditModalOpen] = useState(false)

    // get

    const [incomeCategories, setIncomeCategories] = useState([])

    useEffect( () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {

            fetchCategories()

            return data
        })

    }, []);

    const fetchCategories = () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/incomeCategory', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            // console.log(data.length)
            setIncomeCategories(data)
        })
    }

    // create

    const [incomeCategory, setIncomeCategory] = useState({
        category: ""
    })

    const handleSubmit = e => {
        e.preventDefault()
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/incomeCategory', {
            method: "POST",
            body: JSON.stringify(incomeCategory),
            headers:{
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            setIsModalOpen(false)
            res.json()
        })
        .catch( err => console.log(err));
    }

    const handleChange = e => {
        setIncomeCategory({
            [e.target.id]: e.target.value
        })
    }

    // update

    const [category, setCategory] = useState([])

    const [categoryId, setCategoryId] = useState({})

    const editCategory = (e, id, category) => {
        e.preventDefault()
        setIsEditModalOpen(true)
        setCategoryId(id)
        setCategory(category)

        
    }

    const handleCategoryChange = (e) => {
        setCategory(e.target.value)
    }

    const handleCategorySubmit = (e) => {
        e.preventDefault()

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/incomeCategory/${categoryId}`, {
            method : "PUT",
            body: JSON.stringify({"category": category}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    useEffect(() => {
        fetchCategories()
    }, [incomeCategories])

    // delete

    const deleteCategory = (e, id) => {
        e.preventDefault();
        setIsLoading(false)
        
        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/incomeCategory/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
            setIsLoading(true)
            fetchIncome()
        })
        .catch(err => console.log(err))
    }

    // get income

    const [income, setIncome] = useState([])

    useEffect( () => {
        
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {

            fetchIncome()

            return data
        })

    }, []);

    const fetchIncome = () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/income', {
                headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {
            console.log(data)
            setIncome(data)
        })
    }

    // add income

    const [isAddIncomeModalOpen, setIsAddIncomeModalOpen] = useState(false);
    const [incomeId, setIncomeId] = useState();
    const [incomeAmount, setIncomeAmount] = useState("");
    const [isLoading, setIsLoading] = useState(false);

    const getExpenseCategory = (e) => {
        e.preventDefault()
        setIncomeId(e.target.value)
    }

    const handleIncomeEntrySubmit = (e) => {
        e.preventDefault()
        setIsLoading(false)

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${incomeId}/income`, {
            method: "POST",
            body: JSON.stringify({"amount": incomeAmount}),
            headers:{
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => {
            // setIsModalOpen(false)
            setIsLoading(true)
            res.json()
        })
        .catch( err => console.log(err));
    }

    useEffect(() => {
        if (isLoading) {
            fetchIncome()
        }
    }, [isLoading])

    // edit income

    const [isUpdateIncomeModalOpen, setIsUpdateIncomeModalOpen] = useState(false);
    const [incomeEntry, setIncomeEntry] = useState();
    const [incomeCategoryIdEntry, setIncomeCategoryIdEntry] = useState();
    const [selectedValue, setSelectedValue] = useState();
    const [newAmount, setNewAmount] = useState(0);
    const [entryId, setEntryId] = useState("");


    const editIncomeEntry = (e, id, amount, category, categoryId) => {
        e.preventDefault()
        setIsUpdateIncomeModalOpen(true)
        setIncomeEntry(category)
        setIncomeCategoryIdEntry(categoryId)
        setEntryId(id)
    }

    const setNewCategory = (e) => {
        e.preventDefault()
        setSelectedValue(e.target.value)
    }

    const handleEditEntry = (e) => {
        e.preventDefault()
        setIsLoading(false)

        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${incomeCategoryIdEntry}/income/${entryId}`, {
            method : "PUT",
            body: JSON.stringify({"incomeCategoryId": {"_id": selectedValue}, "amount": newAmount}),
            headers: {
                "Content-Type" : "application/json",
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    // delete entry

    const deleteEntry = (e, id, categoryId) => {
        e.preventDefault();
        setIsLoading(false)
        
        fetch(`https://enigmatic-plateau-21039.herokuapp.com/api/transactions/${categoryId}/income/${id}`, {
            method : "DELETE",
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => {
            setIsLoading(true)
            return res.json()
        })
        .then( data => {
            console.log(data)
        })
        .catch(err => console.log(err))
    }

    return(
        <Container>
            <div className="jumbotron">
            <Row>
            <Col lg={2} className="mb-5">
                <Button onClick={() => setIsModalOpen(true)}>Create Category</Button>
                <Modal isOpen={isModalOpen} onRequestClose={() => setIsModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                    <h2>Title</h2>
                    <Form onSubmit={handleSubmit}>

                        <Form.Group controlId="category">
                            <Form.Label>Category: </Form.Label>
                            <Form.Control 
                                type="text"
                                onChange={handleChange}
                                value={incomeCategory.category}
                            />
                        </Form.Group>
                        
                        <Button type="submit">Add</Button>
                    </Form>
                    <Button onClick={() => setIsModalOpen(false)}>Cancel</Button>
                </Modal>
                </Col>
            </Row>
            <Row>
                <Modal isOpen={isEditModalOpen} onRequestClose={() => setIsEditModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                    <h2>Title</h2>
                    <Form onSubmit={handleCategorySubmit}>

                        <Form.Group controlId="category">
                            <Form.Label>Category: </Form.Label>
                            <Form.Control 
                                type="text"
                                onChange={(event) => handleCategoryChange(event)}
                                value={category}
                            />
                        </Form.Group>
                        
                        <Button type="submit">Update</Button>
                    </Form>
                    <Button onClick={() => setIsEditModalOpen(false)}>Cancel</Button>
                </Modal>
            </Row>
            <Row>
                <Col>
                <table>
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price</th>
                                <th colSpan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                    {incomeCategories.map(expense => {

                        return (
                            <tr key={expense._id}>
                                <td>{expense.category}</td>
                                <td>
                                    <Button variant="warning" onClick={(event) => editCategory(event, expense._id, expense.category)}>Edit</Button>
                                </td>
                                <td>
                                    <Button variant="danger" onClick={(event) => deleteCategory(event, expense._id)}>Delete</Button>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </table>
                </Col>
            </Row>
            </div>
                <div className="jumbotron">
            <Row>
                <Col lg={2} className="mb-5">
                    <Button onClick={() => setIsAddIncomeModalOpen(true)}>Add Income Entry</Button>
                    <Modal isOpen={isAddIncomeModalOpen} onRequestClose={() => setIsAddIncomeModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                        <h2>Title</h2>
                        <Form onSubmit={handleIncomeEntrySubmit}>

                            <Form.Group controlId="category">
                                <Form.Label>Category: </Form.Label>
                                <Form.Control size="sm" as="select" onChange={getExpenseCategory}>
                                    <option></option>
                                    {   incomeCategories.map(income => {
                                            return (
                                                <option value={income._id}>{income.category}</option>
                                            )
                                        })
                                    }
                                </Form.Control>
                            </Form.Group>
                            
                            <Form.Group controlId="amount">
                                <Form.Label>Amount: </Form.Label>
                                <Form.Control 
                                    type="text"
                                    onChange={(event) => setIncomeAmount(event.target.value)}
                                    value={incomeAmount}
                                />
                            </Form.Group>
                            
                            <Button type="submit">Add</Button>
                        </Form>
                        <Button onClick={() => setIsEditModalOpen(false)}>Cancel</Button>
                    </Modal>
                </Col>
            </Row>
            <Row>
                
                 <Modal isOpen={isUpdateIncomeModalOpen} onRequestClose={() => setIsUpdateIncomeModalOpen(false)} style={{ overlay: { backgroundColor: 'grey' }, content: { color: 'orange' } }}>
                    <h2>Title</h2>
                    <Form onSubmit={handleEditEntry}>

                        <Form.Group controlId="category">
                            <Form.Label>Category: </Form.Label>
                            <Form.Control size="sm" as="select" onChange={setNewCategory}>
                                <option></option>
                                {   incomeCategories.map(income => {
                                        return (
                                            <option value={income._id}>{income.category}</option>
                                        )
                                    })
                                }
                            </Form.Control>
                        </Form.Group>
                        
                        <Form.Group controlId="amount">
                            <Form.Label>Amount: </Form.Label>
                            <Form.Control 
                                type="text"
                                onChange={(event) => setNewAmount(event.target.value)}
                                value={newAmount}
                            />
                        </Form.Group>
                        
                        <Button type="submit">Update</Button>
                    </Form>
                    <Button onClick={() => setIsUpdateIncomeModalOpen(false)}>Cancel</Button>
                </Modal>
                <Col>
                    <table>
                        <thead>
                            <tr>
                                <th>Category</th>
                                <th>Price</th>
                                <th colSpan="2">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                    {income.map(income => {

                        return (
                            <tr key={income._id}>
                                <td>{income.amount}</td>
                                <td>{income.incomeCategoryId.category}</td>
                                <td>
                                    <Button variant="warning" onClick={(event) => editIncomeEntry(event, income._id, income.amount, income.incomeCategoryId.category, income.incomeCategoryId._id)}>Edit</Button>
                                </td>
                                <td>
                                    <Button variant="danger" onClick={(event) => deleteEntry(event, income._id, income.incomeCategoryId._id)}>Delete</Button>
                                </td>
                            </tr>
                        )
                    })}
                          </tbody>
                    </table>
                </Col>
            </Row>
            </div>
        </Container>
    )
}