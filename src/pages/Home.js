import { useEffect, useState } from "react";
import { Container, Row, Col, Button, Form } from 'react-bootstrap';

export default function Home() {

    const [incomes, setIncomes] = useState([])

    const [expenses, setExpenses] = useState([])

    const [totalIncomes, setTotalIncomes] = useState(0)

    const [totalExpenses, setTotalExpenses] = useState(0)

    const [incomeCategories, setIncomeCategories] = useState([])

    useEffect( () => {
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then( res => res.json())
        .then( data => {

            fetch('https://enigmatic-plateau-21039.herokuapp.com/api/transactions/incomeCategory', {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then( res => res.json())
            .then( data => {
                console.log(data.length)
                setIncomeCategories(data)
            })
            
            return data
        })

    }, []);
    
    return(
        <div>
            <Container className="mt-5">
                <h1>My Dashboard</h1>
            </Container>
            {/* {incomeCategories.map(expense => {

                return (
                    <tr key={expense._id}>
                        <td>{expense.category}</td>
                        <td><Button variant="warning" block>Edit</Button></td>
                        <td><Button variant="danger" block>Delete</Button></td>
                    </tr>
                )
            })} */}
        </div>
    )
}