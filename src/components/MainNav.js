import { Navbar, Nav, Container, NavDropdown, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useContext } from 'react';
import { ApplicationContext } from './../contexts/ApplicationContext';
import { useHistory } from 'react-router-dom';
import './MainNav.css'

export default function MainNav() {
    const history = useHistory()

    const { user, setUser } = useContext(ApplicationContext);

    const handleClick = () => {
        setUser({
            userId: "",
            isAdmin: false,
            email: "",
            firstName: "",
            lastName: ""
        })

        localStorage.clear()
        history.push("/")
    }

    const navLinks = !user.userId ? 
    <>
        <Nav.Link id="log-in" className="btn btn-outline-success" as={Link} to="/login">Log In</Nav.Link>
        <Nav.Link id="sign-up" className="btn btn-success" as={Link} to="/register">Sign Up</Nav.Link>
    </> : 
    <>
        <NavDropdown title={`${user.firstName} ${user.lastName}`} id="basic-nav-dropdown">
            <NavDropdown.Item as={Link} to="/income">My Income</NavDropdown.Item>
            <NavDropdown.Item as={Link} to="/expense">My Expense</NavDropdown.Item>
        </NavDropdown>
        <Nav.Link onClick={handleClick}>Logout</Nav.Link>
    </>

    return (
        <Navbar className="nav" expand="md">
            <Container>
                <Navbar.Brand as={Link} to="/"><img id="logo" src="./../../trackFund-logo.png" alt="Girl in a jacket"/></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="ml-auto">
                    {navLinks}
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}