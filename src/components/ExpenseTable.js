import ExpenseRow from './CategoryRow';

export default function ExpenseTable({ expenseCategories, editExpenseCategory, deleteExpenseCategory }) {

    return (
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Category</th>
                        <th colSpan="2">Action</th>
                    </tr>
                </thead>
                <tbody>
            {expenseCategories.map(expense => {

                return (
                    <ExpenseRow taransactionCategory={expense} editExpenseCategory={editExpenseCategory} deleteExpenseCategory={deleteExpenseCategory} />
                )
            })}
                </tbody>
            </table>
        </div>
    )

}