import { useHistory } from 'react-router-dom';
import { useState } from 'react'
import { Form, Button, Card} from 'react-bootstrap'
import './RegisterForm.css'

export default function RegisterForm() {
    const history = useHistory()
    
    const [credentials, setCredentials] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        confirmPassword: "",
        // mobileNo: "",
        // isAdmin: false
    })

    const [isLoading, setIsLoading] = useState(false)
    
    const handleSubmit = e => {
        e.preventDefault()
        setIsLoading(true)
        fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users/', {
            method: "POST",
            body: JSON.stringify(credentials),
            headers:{
                "Content-Type" : "application/json"
            }
        })
        .then( res => {
            console.log(res)
            setIsLoading(false)
            if(credentials.password !== credentials.confirmPassword){
                alert("Password doesnt match")
            } else {
                if (res.status === 200) {
                    alert("Register successful,Redirecting to Login Page")
                    history.push("/login")
                    return res.json()
                } else {
                    alert("Register Unsuccessful");
                    throw new Error("Register Unsuccessful");
                }
            }
        })
        .catch( err => console.log(err));
    }

    const handleChange = e => {
        setCredentials({
            ...credentials,
            [e.target.id]: e.target.value
            
        })
    }

    return (
        <Card class="card">
            <Form onSubmit={handleSubmit}>

                <Form.Group controlId="firstName">
                    <Form.Label>First Name:</Form.Label>
                    <Form.Control type="text" onChange={handleChange}
                    value={credentials.firstName} />
                </Form.Group>

                <Form.Group controlId="lastName">
                    <Form.Label>Last Name:</Form.Label>
                    <Form.Control type="text" onChange={handleChange}
                    value={credentials.lastName} />
                </Form.Group>

                <Form.Group controlId="email">
                    <Form.Label>Email:</Form.Label>
                    <Form.Control type="email" onChange={handleChange}
                    value={credentials.email} />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control type="password" onChange={handleChange}
                    value={credentials.password} />
                </Form.Group>

                <Form.Group controlId="confirmPassword">
                    <Form.Label>Confirm Password:</Form.Label>
                    <Form.Control type="password" onChange={handleChange}
                    value={credentials.confirmPassword} />
                </Form.Group>

                {/* <Form.Group controlId="mobileNo">
                    <Form.Label>Mobile No:</Form.Label>
                    <Form.Control type="text" onChange={handleChange}
                    value={credentials.mobileNo} />
                </Form.Group> */}

                {isLoading ?
                    <Button type="submit" disabled>Register</Button>
                    : <Button type="submit" >Register</Button>
                }
            </Form>
        </Card>

        
    )
}