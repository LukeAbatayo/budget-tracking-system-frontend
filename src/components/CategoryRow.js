import { Button } from 'react-bootstrap';

export default function ExpenseRow({ transactionCategory, editExpenseCategory, deleteExpenseCategory }) {

    return (
        <tr key={transactionCategory._id}>
            <td>{transactionCategory.category}</td>
            <td>
                <Button variant="warning" onClick={(event) => editExpenseCategory(event, transactionCategory._id, transactionCategory.category)}>Edit</Button>
            </td>
            <td>
                <Button variant="danger" onClick={(event) => deleteExpenseCategory(event, transactionCategory._id)}>Delete</Button>
            </td>
        </tr>
    )
}