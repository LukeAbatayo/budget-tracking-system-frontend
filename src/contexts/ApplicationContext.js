import { createContext, useState, useEffect } from "react";

export const ApplicationContext = createContext();

export default function MainNav(props) {

    const [user, setUser] = useState({
        userId: "",
        isAdmin: false,
        email: "",
        firstName: "",
        lastName: ""
    })

   
    useEffect( () => {

        if (!localStorage.token) {

        } else {
            fetch('https://enigmatic-plateau-21039.herokuapp.com/api/users', {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
                const { firstName, lastName, email, isAdmin } = data
                setUser({
                    userId: data._id,
                    firstName,
                    lastName,
                    email,
                    isAdmin
                })
            })
            .catch(err => console.log(err))
        }
    }, [])


    return (
        <ApplicationContext.Provider value={{user, setUser}}>
            {props.children}
        </ApplicationContext.Provider>
    ) 
    
}